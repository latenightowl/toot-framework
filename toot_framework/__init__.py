# SPDX-FileCopyrightText: 2022-present @latenightowl
#
# SPDX-License-Identifier: MIT
import os
import datetime
from pathlib import Path
from typing import List, Optional, Tuple, Union

from loguru import logger

from mastodon import Mastodon


class Message:
    """Message object."""

    def __init__(self, text: str, *, tags: Optional[List[str]] = None):
        self.text = text
        self._tags: List[str] = tags or []

    @property
    def tags(self) -> str:
        return " ".join(f"#{tag}" for tag in self._tags)

    def __str__(self) -> str:
        result: str = self.text
        if self.tags:
            result += "\n\n" + self.tags
        return result


class Attachment:
    """Attachment object."""

    def __init__(
        self,
        path: Union[str, Path],
        *,
        description: Optional[str] = None,
        focus: Optional[Tuple[float, float]] = None,
    ):
        self.path: Path = (path if type(path) is Path else Path(path)).absolute()
        if not self.path.is_file():
            raise RuntimeError(f"Attachment '{self.path}' does not exist.")

        self.description = description
        self.focus = focus


class App:
    """Main application handling login credentials, and basic posting."""

    _config_directory: Path

    def _ensure_config_directory(self) -> None:
        path: str = os.getenv("XDG_CONFIG_HOME", "~/.config")
        identity: str = os.getenv("TOOT_IDENTITY", "")

        appdir: str = "toot-framework." + self.safe_name
        if identity:
            appdir += f".{identity}"

        directory = Path(path).expanduser() / appdir
        directory.mkdir(exist_ok=True)
        logger.debug(f"Using config directory {directory!s}.")
        self._config_directory = directory

    @property
    def _client_credentials_file(self) -> Path:
        return self._config_directory / "client.secret"

    @property
    def _user_credentials_file(self) -> Path:
        return self._config_directory / "user.secret"

    @property
    def _raw_credentials(self) -> None:
        """Recover server, email and password."""
        secrets = {
            "server": None,
            "email": None,
            "password": None,
            "_changed": False,
        }

        missing: List[str] = []
        for var in [secret for secret in secrets.keys() if not secret.startswith("_")]:
            envvar: str = f"TOOT_{var.upper()}"

            file: Path = self._config_directory / f"{envvar}.raw-secret"
            file_content: str = ""
            if file.is_file():
                with file.open("r") as handle:
                    logger.debug(f"Found {envvar} in file {file!s}.")
                    file_content = handle.read()

            # Environment variable is specified and is the same as in cache
            envval: str = os.getenv(envvar, "")
            if envvar == file_content:
                secrets[var] = envvar
                continue

            # Environment variable is specified and is different from the cache
            if envval and envvar != file_content:
                # The value has been updated, probably
                logger.debug(f"Updating contents of {file!s}.")
                with file.open("w") as handle:
                    handle.write(envval)
                secrets[var] = envval
                secrets["_changed"] = True
                continue

            # Environment variable is not specified and is in the cache
            if file_content:
                secrets[var] = file_content
                continue

            # We don't know the value
            logger.critical(f"Missing raw secret {envvar}.")
            missing.append(envvar)

        if missing:
            raise RuntimeError(
                "Could not recover secrets: " + ", ".join(missing) + ". "
                "Please, specify them before starting the application."
            )

        return secrets

    def __init__(self, name: str, language: str, visibility: str = "public"):
        """Create application.

        :param name: Name of the application.
            Will be visible on Mastodon.
            It will also be used as a directory to store appliation data.
        :param language: Two-letter ISO 639-1 code.
        :param visibility: 'public', 'unlisted', 'private' or 'direct'.
            This settings will be used for post visibility.
            Setting 'TOOT_DEBUG' environment variable overwrites this to 'direct'.
        """
        self.name = name
        self._ensure_config_directory()

        self.language = language
        raw_credentials = self._raw_credentials
        self.server = raw_credentials["server"]
        self._email = raw_credentials["email"]
        self._password = raw_credentials["password"]

        if not self._client_credentials_file.is_file():
            logger.debug("Client credentials do not exist, creating.")
            Mastodon.create_app(
                self.name,
                api_base_url=self.server,
                to_file=self._client_credentials_file.absolute(),
            )
            logger.debug("Client credentials created.")

        self.debug: bool = len(os.getenv("TOOT_DEBUG", "")) > 0
        if self.debug:
            logger.info("Running in debug mode. Toots won't be visible to the public.")

        logger.debug("Creating application.")
        self._app = Mastodon(
            client_id=self._client_credentials_file.absolute(),
            api_base_url=self.server,
        )
        logger.debug("Logging in...")
        self._app.log_in(
            self._email,
            self._password,
            to_file=self._user_credentials_file.absolute(),
        )

        me = self._app.me()
        logger.info(
            "Logged in as @{user}@{server}.".format(
                user=me["username"], server=self.server.lstrip("htps:/")
            )
        )

    @property
    def safe_name(self) -> str:
        return self.name.lower().replace(" ", "-")

    # Public API

    def toot(
        self,
        message: Message,
        *,
        language: Optional[str] = None,
        attachments: Optional[List[Attachment]] = None,
        at: Optional[datetime.datetime] = None,
    ) -> dict:
        """Post a toot.

        :param message: Message content.
        :param language: Language; this will overwrite app's configured language.
        :param attachments: List of attachments.
        :param at: Postpone the post to the given time.
        :return: Post dictionary as per mastodonplus.py documentation.

        If the application has been started in debug mode, it will be sent
        as a direct message, to make it invisible to everyone except the account
        itself.
        """
        if not attachments:
            attachments = []
        elif len(attachments) > 4:
            raise RuntimeError("It is not possible to upload over four attachments.")

        posted_attachments: List[dict] = []
        for attachment in attachments:
            posted_attachment: dict = self._app.media_post(
                str(attachment.path),
                description=attachment.description,
                focus=attachment.focus,
                # TODO Use asynchronous way and check for the status
                synchronous=True,
            )
            posted_attachments.append(posted_attachment)

        visibility: str = "direct" if self.debug else "public"
        logger.debug("Tooting invisibly..." if self.debug else "Tooting...")

        result = self._app.status_post(
            message,
            language=self.language,
            visibility=visibility,
            media_ids=posted_attachments,
            scheduled_at=at,
        )
        logger.debug("Toot!")
        return result
