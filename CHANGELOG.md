# CHANGELOG

**0.2.1 (2022-12-01)**

- Ensure posts are uploaded before the post is sent.

**0.2.0 (2022-11-28)**

- Fix `Message.__str__` for empty tag lists
- Include user information on login
- Add support for multiple accounts using `TOOT_IDENTITY`
- Add `at=` argument to support scheduling

**0.1.0 (2022-11-08)**

- Attachment support

**0.0.0 (2022-11-07)**

- Initial release
