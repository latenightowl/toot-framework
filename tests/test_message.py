from toot_framework import Message


def test_Message__attrs():
    text = "Foo bar."
    tags = ["foo", "bar"]
    message = Message(text, tags=tags)
    assert message.text == text
    assert message.tags == "#foo #bar"


def test_Message__str():
    text = "Foo bar."
    tags = ["foo", "bar"]
    message = Message(text, tags=tags)
    assert str(message) == "Foo bar.\n\n#foo #bar"


def test_Message__str__empty_tags():
    text = "Foo bar."
    tags = []
    message = Message(text, tags=tags)
    assert str(message) == "Foo bar."
