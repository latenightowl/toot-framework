# toot-framework

-----

**Table of Contents**

- [About](#about)
- [Installation](#installation)
- [License](#license)

## About

This is a "framework" that I use to write Mastodon bots:

- [toot-calendar](https://codeberg.org/latenightowl/toot-calendar)

There is no community and thus no documentation.
Look through the code, and the code of the bots, and see if it could suit you as well.
Feel free to reach out if you have ideas or issues.

## Installation

You will most likely want to add this into your `pyproject.toml` as a dependency.

To use this library, add it like so:

```ini
[project]
...
dependencies = [
  "toot-framework @ git+https://codeberg.org/latenightowl/toot-framework.git",
  ...
]
```

## Usage

This library provides some bits that are helping me with development of other applications.

By supplying `TOOT_IDENTITY=something` you can have multiple applications tooting using the same code. It will create another directory in `~/.config`.


## License

`toot-framework` is distributed under the terms of the [MIT](https://spdx.org/licenses/MIT.html) license.
